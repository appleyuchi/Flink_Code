import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.DataSet;

import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.PartitionOperator;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;

public class Rebalance
{


    public static void main(String[] args) throws Exception
    {



        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        String rootPath = new StringBuilder(System.getProperty("user.dir")).append("/dataset_api/Java").toString();


//        DataSet<Tuple2<String,String>> in = env.readCsvFile("file:///"+rootPath+"/"+"mapper.csv").types(String.class,String.class);
        DataSet<Tuple1<String>> in = env.readCsvFile("file:///"+rootPath+"/"+"mapper.csv").types(String.class);


        PartitionOperator<Tuple1<String>> test = in.rebalance();//这个算子是用来减缓数据倾斜的
        test.print();
        DataSet<Tuple2<String, String>> out = in.rebalance().map(new MapFunction<Tuple1<String>, Tuple2<String, String>>()
        {
            @Override
            public Tuple2<String, String> map(Tuple1<String> tuple1) throws Exception {
                return Tuple2.of(tuple1.f0,tuple1.f0);
            }
        });
        out.print();





    }
}


