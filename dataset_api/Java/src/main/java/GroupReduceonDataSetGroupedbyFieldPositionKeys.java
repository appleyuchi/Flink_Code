import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple3;

public class GroupReduceonDataSetGroupedbyFieldPositionKeys
{

    public static void main(String[] args) throws Exception {
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        String rootPath = new StringBuilder(System.getProperty("user.dir")).append("/dataset_api/Java").toString();
        DataSet<Tuple2<Integer, String>> input = env.readCsvFile("file:///"+rootPath+"/"+"group.csv").types(Integer.class, String.class);
        System.out.println("--------------输入数据集-------------");
        input.print();


        DataSet<Tuple2<Integer, String>> output = input.groupBy(0)            // group DataSet by the first tuple field
                .reduceGroup(new DistinctReduce());  // apply GroupReduceFunction


        output.print();

    }

}
