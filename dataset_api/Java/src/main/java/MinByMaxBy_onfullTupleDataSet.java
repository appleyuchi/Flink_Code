import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.DataSet;

public class MinByMaxBy_onfullTupleDataSet {

    public static void main(String[] args) throws Exception {


        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        String rootPath = new StringBuilder(System.getProperty("user.dir")).append("/dataset_api/Java").toString();
        DataSet<Tuple3<Integer, String, Double>> input = env.readCsvFile("file:///"+rootPath+"/"+"aggregate.csv").types(Integer.class, String.class,Double.class);
        input.print();

        System.out.println("-------------------------------------------------");
        DataSet<Tuple3<Integer, String, Double>> output = input.maxBy(0, 2); // select tuple with maximum values for first and third field.
        output.print();
//key0最大的值汇聚,然后在上述结果中返回key2最大的一组数据
//        相当于二次排序
    }


}