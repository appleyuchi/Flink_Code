import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;



public class DefaultJoin {


    public static void main(String[] args) throws Exception {

        User user1=new User("yuchi",315200);
        User user2=new User("chiyu",615000);
        Store store1=new Store("ShenZhen",615000);
        Store store2=new Store("Ningbo",315200);
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        DataSet<User> input1 =env.fromElements(user1,user2);
        DataSet <Store> input2 = env.fromElements(store1,store2);




    // result dataset is typed as Tuple2
    DataSet<Tuple2<User, Store>>
            result = input1.join(input2)
            .where("zip")       // key of the first input (users)
            .equalTo("zip");    // key of the second input (stores)


        result.print();

}
}
