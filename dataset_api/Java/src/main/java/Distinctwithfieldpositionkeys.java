import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.DataSet;


public class Distinctwithfieldpositionkeys {


    public static void main(String[] args) throws Exception {


        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        String rootPath = new StringBuilder(System.getProperty("user.dir")).append("/dataset_api/Java").toString();

        DataSet<Tuple3<Integer, Double, String>> input = env.readCsvFile("file:///" + rootPath + "/" + "projection.csv").types(Integer.class, Double.class, String.class);
        input.print();
        System.out.println("------------------------------------------------------");
        DataSet<Tuple3<Integer, Double, String>> output = input.distinct(0,2);
//把key0和key2组合视为一个整体进行去重.

        output.print();


    }

}