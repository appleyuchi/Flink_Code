import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple3;


import org.apache.flink.api.common.functions.GroupCombineFunction;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.util.Collector;
import org.apache.flink.api.java.tuple.Tuple2;


public class CombinableGroupReduceFunctions {
    public static void main(String[] args) throws Exception {
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        String rootPath = new StringBuilder(System.getProperty("user.dir")).append("/dataset_api/Java").toString();
        DataSet<Tuple2<String,Integer>> in = env.readCsvFile("file:///"+rootPath+"/"+"cog1.csv").types(String.class, Integer.class);
        System.out.println("-------------------输出in看下--------------------------");
        in.print();
        System.out.println("-------------------reduceGroup内容-------------------------");
        in.reduceGroup(new MyCombinableGroupReducer()).print();
        System.out.println("-------------------combineGroup内容-------------------------");
        in.combineGroup(new MyCombinableGroupReducer()).print();

    }


}


