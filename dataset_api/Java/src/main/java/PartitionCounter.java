
import org.apache.flink.api.common.functions.MapPartitionFunction;
import org.apache.flink.util.Collector;

public class PartitionCounter implements MapPartitionFunction<String, Long> {

    public void mapPartition(Iterable<String> values, Collector<Long> out) {
        long c = 0;
        for (String s : values) {
            c++;
        }
        out.collect(c);
    }
}