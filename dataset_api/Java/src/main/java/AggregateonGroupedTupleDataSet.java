
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.ExecutionEnvironment;

import org.apache.flink.api.*;

import static org.apache.flink.api.java.aggregation.Aggregations.*;

public class AggregateonGroupedTupleDataSet {
    public static void main(String[] args) throws Exception {
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        String rootPath = new StringBuilder(System.getProperty("user.dir")).append("/dataset_api/Java").toString();
        DataSet<Tuple3<Integer, String,Double>> input =env.readCsvFile("file:///"+rootPath+"/"+"aggregate.csv").types(Integer.class, String.class,Double.class);


        input.print();
        System.out.print("--------------------------------------------------\n");
        DataSet<Tuple3<Integer, String, Double>> output = input
                .groupBy(1)        // group DataSet on second field
                .aggregate(SUM, 0) // compute sum of the first field
                .and(MAX, 2);      // compute minimum of the third field

        output.print();
        //对第一个key进行求和,对第三个key保留第一个
        //根据第二个key进行聚合
//        select name,sum(nameid),max(tenant_id) from test group by name

    }
}
