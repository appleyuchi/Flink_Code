import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.DataSet;


// MapFunction that adds two integer values




class Map {
    public static void main(String[] args) throws Exception {

        ExecutionEnvironment senv = ExecutionEnvironment.getExecutionEnvironment();

        System.out.println("---------------------------------");
        String rootPath = new StringBuilder(System.getProperty("user.dir")).append("/dataset_api/Java").toString();
        System.out.println(rootPath);

        DataSet<Tuple2<Integer, Integer>> intPairs =senv.readCsvFile("file:///"+rootPath+"/"+"map.csv").types(Integer.class,Integer.class );
        intPairs.print();
        DataSet<Integer> intSums = intPairs.map(new IntAdder());
        intSums.print();

    }

}






