import org.apache.flink.api.common.functions.MapPartitionFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

public class RangePartition
{



    public static void main(String[] args) throws Exception
    {


        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        String rootPath = new StringBuilder(System.getProperty("user.dir")).append("/dataset_api/Java").toString();
        DataSet<Tuple2<String,Integer>> in = env.readCsvFile("file:///"+rootPath+"/"+"cog1.csv").types(String.class,Integer.class);

// range-partition DataSet by String value and apply a MapPartition transformation.
        DataSet<Tuple2<String, String>> out = in.partitionByRange(0)
                .mapPartition(new MapPartitionFunction<Tuple2<String, Integer>, Tuple2<String, String>>() {
                    @Override
                    public void mapPartition(Iterable<Tuple2<String, Integer>> iterable, Collector<Tuple2<String, String>> out) throws Exception
                    {


                        String key = "key_default";
                        String sum = "sum_default";

//                    下面的代码只是为了跑通和补全官方文档中的例子，并没有多大的实际意义。
                        for (Tuple2< String,Integer> curr : iterable)
                        {
                            key = curr.f0.toString();
                            sum =sum + curr.f1.toString();
                        }
                        // emit tuple with key and sum
                        out.collect(new Tuple2<>(key, sum));



                    }
                });

        out.print();


    }

}
