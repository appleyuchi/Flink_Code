import org.apache.flink.api.common.functions.FilterFunction;
public class NaturalNumberFilter implements FilterFunction<Integer> {
    @Override
    public boolean filter(Integer number) {
        return number >= 3;
    }
}