import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.common.operators.Order;
public class GroupReduceonsortedgroups {


    public static void main(String[] args) throws Exception {

        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        String rootPath = new StringBuilder(System.getProperty("user.dir")).append("/dataset_api/Java").toString();
        DataSet<Tuple2<Integer, String>> input = env.readCsvFile("file:///"+rootPath+"/"+"group.csv").types(Integer.class, String.class);


        input.print();
        System.out.println("-----------------------------------------------");


        DataSet<Tuple2<Integer,String>> output = input.groupBy(0)                         // group DataSet by first field
                        .sortGroup(1, Order.ASCENDING)      // sort groups on second tuple field
                        .reduceGroup(new DistinctReduce2());//对每组配合进行去重



        output.print();


    }
}