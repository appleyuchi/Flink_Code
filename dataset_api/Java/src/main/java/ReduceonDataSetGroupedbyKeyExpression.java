import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;

class ReduceonDataSetGroupedbyKeyExpression {
    public static void main(String[] args) throws Exception {


        ExecutionEnvironment senv = ExecutionEnvironment.getExecutionEnvironment();
        WC wc1 = new WC("test", 1);
        WC wc2 = new WC("test", 2);

        DataSet<WC> words = senv.fromElements(wc1, wc2);
        words.print();
        System.out.println("-------------------------------");
        DataSet<WC> wordCounts = words
                // DataSet grouping on field "word"
                .groupBy("word")
                // apply ReduceFunction on grouped DataSet
                .reduce(new WordCounter());


        wordCounts.print();
    }
}