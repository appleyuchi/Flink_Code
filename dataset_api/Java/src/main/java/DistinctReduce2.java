import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;
import org.apache.flink.api.common.functions.GroupReduceFunction;

public class DistinctReduce2 implements GroupReduceFunction<Tuple2<Integer, String>, Tuple2<Integer, String>> {
    @Override
    public void reduce(Iterable<Tuple2<Integer, String>> in, Collector<Tuple2<Integer, String>> out) {
        Integer key = null;
        String comp = null;

        for (Tuple2<Integer, String> t : in)
        {
            key = t.f0;
            String next = t.f1;

            // check if strings are different
            if (comp == null || !next.equals(comp))
            {
                out.collect(new Tuple2<Integer, String>(key, next));//这里是在进行返回
                comp = next;
            }
        }
    }
}