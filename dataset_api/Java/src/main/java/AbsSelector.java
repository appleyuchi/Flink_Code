import org.apache.flink.api.java.functions.KeySelector;
public class AbsSelector implements KeySelector<Integer, Integer> {
    private static final long serialVersionUID = 1L;
    @Override
    public Integer getKey(Integer t) {
        System.out.println("Math.abs(t)="+Math.abs(t));
        return Math.abs(t);
    }
}