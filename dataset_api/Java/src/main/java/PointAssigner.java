import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.common.functions.JoinFunction;


//这个入口参数的意思是:
//<Tuple2<String, String>
//Rating2
//返回参数为:
//Tuple2<String, Double>>

//这句implements是指定入口参数和返回参数类型
public class PointAssigner implements JoinFunction<Tuple2<String, String>, Rating, Tuple2<String, Integer>> {
@Override
    public Tuple2<String, Integer> join(Tuple2<String, String> movie, Rating rating) {
        // Assigns the rating points to the movie.
        // NOTE: rating might be null

        return new Tuple2<String, Integer>(movie.f0, rating == null ? -1 : rating.points);
    }
}