import org.apache.flink.api.common.functions.ReduceFunction;
public class WordCounter implements ReduceFunction<WC> {
    @Override
    public WC reduce(WC in1, WC in2) {
        return new WC(in1.word, in1.count + in2.count);
    }
}