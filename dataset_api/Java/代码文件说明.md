需要用到的数据集列表:

				
|  数据格式 | 数据集文件名 |
|  ----  | ----  |
|DataSet &lt;Tuple2&lt;Integer,Double&gt;&gt;|tupledata.csv|
|DataSet&lt;Tuple2&lt;Integer,Integer&gt;&gt;|map.csv|
|DataSet&lt;Tuple2&lt;String,Double&gt;&gt;|flatjoin.csv|
|DataSet&lt;Tuple2&lt;String,Integer&gt;&gt;|cog1.csv|
|DataSet&lt;Tuple2&lt;String,String&gt;&gt;|flatmap.csv|
|DataSet&lt;Tuple2&lt;Integer,String&gt;&gt;|group.csv<br>group1.csv|
|DataSet&lt;Tuple3&lt;String,Integer,Double&gt;&gt;|reduce.csv|
|DataSet&lt;Tuple3&lt;Integer,Byte,String&gt;&gt;|byte.csv|
|DataSet &lt;Tuple3&lt;Double,String,Double&gt;&gt;|aggregate.csv|
|DataSet&lt;Tuple3&lt;Integer,Double,String&gt;&gt;|projection.csv|
|DataSet&lt;Tuple5&lt;String,String,String,String,String&gt;&gt;|hint.csv|
|DataSet&lt;String&gt;|mapper.csv|
|DataSet&lt;Integer&gt;|number.csv|

注:<br>
上述数据文件是为了满足不同的



				
|  用法   | 执行前的数据类型  |执行前的数据|执行后的数据类型|执行后的数据
|  ----  | ----  |----  |----  |----  |
| [Map](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/Map.java)  |DataSet|(1,2)<br>(3,4)|DataSet|7<br>3
|[FlatMap](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/FlatMap.java)|DataSet|"India", "USA"|DataSet|India<br>USA
|[MapPartition](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/MapPartition.java)|DataSet|"India", "USA","China","America"|DataSet|4
|[Filter](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/Filter.java)|DataSet|1,2,3,4,5,6|DataSet|3,4,5,6
|[Projection of Tuple DataSet](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/Projection.java)|DataSet|(3,3.62,"test2")<br>(8,5.63,"test3")<br>(7,5.63,"test3")<br>(7,5.63,"test3")<br>(8,1.61,"test1")<br>(8,3.67,"test1")<br>|DataSet|("test3",7)<br>("test3",7)<br>("test3",8)<br>("test2",3)<br>("test1",8)<br>("test1",8)<br>
|[Projection of Tuple DataSet](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/Projection.java)|DataSet|(3,3.62,"test2")<br>(8,5.63,"test3")<br>(7,5.63,"test3")<br>(7,5.63,"test3")<br>(8,1.61,"test1")<br>(8,3.67,"test1")<br>|DataSet|("test6")<br>("test11")<br>("test1")<br>
|[Reduce on DataSet Grouped by Key Expression](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/ReduceonDataSetGroupedbyKeyExpression.java)|DataSet|test,1<br>test,2|DataSet|test,3
|[Reduce on DataSet Grouped by KeySelector Function](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/ReduceonDataSetGroupedbyKeySelectorFunction.java)|DataSet|test,1<br>test,2|DataSet|test,3
|[Reduce on DataSet Grouped by Field Position Keys (Tuple DataSets only)](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/ReduceonDataSetGroupedbyFieldPositionKeys.java)|DataSet|"study",6,4.56<br>"study",4,7.56<br>"study",4,7.56<br>"study",6,4.56|DataSet|("study",8,15.12)<br>("study",12,9.12)|
|Reduce on DataSet grouped by Case Class Fields|官方不支持|官方不支持|官方不支持|官方不支持|
|[GroupReduce on DataSet Grouped by Field Position Keys](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/GroupReduceonDataSetGroupedbyFieldPositionKeys.java)|DataSet|(44465,chiyu)<br>(6655,test)<br>(1265,chiyu)<br>(1265,chiyu)<br>(1265,yuchi)<br>(1265,chiyu)|DataSet|(6655,test)<br>(1265,chiyu)<br>(1265,yuchi)<br>(44465,chiyu)
|[GroupReduce on sorted groups](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/GroupReduceonsortedgroups.java)|DataSet|(1265,chiyu)<br>(44465,chiyu)<br>(1265,yuchi)<br>(6655,test)<br>(1265,chiyu)<br>(1265,chiyu)|DataSet|(6655,test)<br>(1265,chiyu)<br>(1265,yuchi)<br>(44465,chiyu)
|[Combinable GroupReduceFunctions](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/CombinableGroupReduceFunctions.java)|DataSet|(walk,4)<br>(walk,5)|DataSet|walk-9<br>(walk,9)|
|[GroupCombine on a Grouped DataSet](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/GroupCombineonaGroupedDataSet.java)|DataSet|与上面类似，只是把算子写在new中|DataSet|与上面类似，只是把算子写在new中|
|[Aggregate on Grouped Tuple DataSet](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/AggregateonGroupedTupleDataSet.java)|DataSet|(9,chiyu,3.0)<br>(1,yuchi,1.0)<br>(3,chiyu,4.0)<br>(9,chiyu,2.0)<br>(3,yuchi,6.0)<br>(2,yuchi,5.0)|DataSet|(6,yuchi,1.0)<br>(21,chiyu,2.0)
|[MinBy / MaxBy on Grouped Tuple DataSet](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/MinByMaxBy_onGroupedTupleDataSet.java)|DataSet|(9,chiyu,2.0)<br>(3,chiyu,4.0)<br>(2,yuchi,5.0)<br>(3,yuchi,6.0)<br>(9,chiyu,3.0)<br>(1,yuchi,1.0)|DataSet|(1,yuchi,1.0)<br>(3,chiyu,4.0)|
|[Reduce on full DataSet](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/ReduceonfullDataSet.java)|DataSet|1,2,3|DataSet|6
|[GroupReduce on full DataSet](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/GroupReduceonfullDataSet.java)|DataSet|7 6 4 3|DataSet|20|
|[Aggregate on full Tuple DataSet](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/AggregateonfullTupleDataSet.java)|DataSet|(2,3.5)<br>(1,3.4)<br>(3,3.4)<br>(2,3.5)|DataSet|(8,3.4)
|[MinBy / MaxBy on full Tuple DataSet](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/MinByMaxBy_onfullTupleDataSet.java)|DataSet|(3,yuchi,6.0)<br>(9,chiyu,2.0)<br>(2,yuchi,5.0)<br>(9,chiyu,3.0)<br>(3,chiyu,4.0)<br>(1,yuchi,1.0)<br>|(9,chiyu,3.0)|DataSet|
|[Distinct](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/Distinct.java)|DataSet|(2,3.5)<br>(3,3.4)<br>(1,3.4)<br>(2,3.5)|DataSet|(3,3.4)<br>(1,3.4)<br>(2,3.5)
|[Distinct with field position keys](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/Distinctwithfieldpositionkeys.java)|DataSet|(8,1.61,"test1")<br>(3,3.62,"test2")<br>(8,5.63,"test3")<br>(7,5.63,"test3")<br>(7,5.63,"test3")<br>(8,3.67,"test1")|DataSet|(8,3.67,"test1")<br>(7,5.63,"test3")<br>(3,3.62,"test2")<br>(8,5.63,"test3")
|[Distinct with KeySelector function](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/DistinctwithKeySelectorfunction.java)|DataSet|-2,2,3|DataSet|3,-2|
|[Distinct with key expression](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/Distinctwithkeyexpression.java)|DataSet|读取数据变成一个类|DataSet|读取数据变成一个类|
|[DefaultJoin](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/DefaultJoin.java)|DataSset|User user1=new User("yuchi",315200);<br>User user2=new User("chiyu",615000);<br>Store store1=new Store("ShenZhen",615000);<br>Store store2=new Store("Ningbo",315200);|DataSet|(chiyu,615000,ShenZhen,615000)<br>(yuchi,315200,Ningbo,315200)|
|[Join with Join Function](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/JoinwithJoinFunction.java)|DataSet|ratings=<br>Rating("yuchi","run",50);<br>weights =<br>run,1.56<br>run,2.43<br>walk,1.56<br>walk,2.43|DataSet|(yuchi,121.50000000000001)<br>(yuchi,78.0)
|[Join with Flat-Join Function](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/JoinwithFlatJoinFunction.java)|DataSet|ratings=<br>Rating("yuchi","run",50);<br>weights =<br>run,1.56<br>run,2.43<br>walk,1.56<br>walk,2.43|DataSet|(yuchi,121.50000000000001)<br>(yuchi,78.0)
|[Join with Projection (Java Only)](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/JoinwithProjection.java)|DataSet|有条件的join(根据指定的key)|DataSet|有条件的join(根据指定的key)|
|[Join with DataSet Size Hint](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/JoinwithDataSetSizeHint.java)|DataSset|(44465,chiyu)<br>(1265,chiyu)<br>(1265,chiyu)<br>(1265,yuchi)<br>(1265,chiyu)<br>(6655,test)|DataSet|((1265,yuchi),(1265,yuchi2))<br>((1265,chiyu),(1265,yuchi2))<br>((1265,chiyu),(1265,yuchi2))<br>((1265,chiyu),(1265,yuchi2))
|[Join Algorithm Hints](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/JoinAlgorithmHints.java)|DataSet|两个dataset根据第0列数据进行join操作|DataSet|两个dataset根据第0列数据进行join操作
|[OuterJoin with Join Function](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/OuterJoinwithJoinFunction.java)|DataSet|rating1=(HarryPotter","Humor",10)<br>movies=(HarryPotter,Titanic)|DataSet|(HarryPotter,10)|
|[OuterJoin with Flat-Join Function](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/OuterJoinwithFlatJoinFunction.java)|DataSet|rating1=(HarryPotter","Humor",10)<br>movies=(HarryPotter,Titanic)|DataSet|(HarryPotter,10)||
|JoinAlgorithmHint2|||||
|[Cross with User-Defined Function](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/CrosswithUserDefinedFunction.java)|DataSet|1,35,67<br>5,98,27|DataSet|1,5,74.62573282722254|
|[Cross with Projection](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/CrosswithProjection.java)|DataSet|Cross Join(笛卡尔积)|DataSet|Cross Join(笛卡尔积)
|[Cross with DataSet Size Hint](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/CrosswithDataSetSizeHint.java)|DataSet|计算笛卡尔积(排列组合)|DaSet|计算笛卡尔积(排列组合)
|[CoGroup on DataSets](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/CoGrouponDataSets.java)|DataSset|iVals=<br>walk,4<br>walk,5<br>dVals=<br>run,1.56<br>run,2.43<br>walk,1.56<br>walk,2.43|DataSet|9.72<br>12.15<br>6.24<br>7.800000000000001
|[Union](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/Union.java)|DataSet|-|DataSet|汇集所有数据
|[Reblance](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/Rebalance.java)|DataSet|(world)|DataSet|(world,world)改善数据倾斜|
|[Hash-Partition](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/HashPartition.java)|DataSet|partition数据均匀|DataSet|让partition数据均匀|
|[Range-Partition](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/RangePartition.java)|DataSet|指定每个partition的数据量|DataSet|指定每个partition的数据量|
|[Sort-Partition](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/SortPartition.java)|DataSet|对partition中的数据，<br>根据某列进行排序|DataSet|对partition中的数据，<br>根据某列进行排序|
|[First-n](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/src/main/java/Firstn.java)|DataSet|-|DataSet|返回前面几条数据||



Reference:<br>
[DataSet 官方文档](https://ci.apache.org/projects/flink/flink-docs-stable/dev/batch/dataset_transformations.html)
