import com.alibaba.fastjson.JSON;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



//---------------------------List<User>->json字符串---------------------------
public class List_User_json {
    public static void main(String[] args) throws FileNotFoundException, IOException {

        User user1 = new User("zhangsan", "123123");
        User user2 = new User("lisi", "321321");
        List<User> users = new ArrayList<User>();
        users.add(user1);
        users.add(user2);

        String ListUserJson = JSON.toJSONString(users);
        System.out.println("List<Object>转json字符串:" + ListUserJson);
//---------------------------List<User>转json字符串---------------------------
        UserGroup userGroup = new UserGroup("userGroup", users);
        String userGroupJson = JSON.toJSONString(userGroup);
        System.out.println("复杂java类转json字符串:" + userGroupJson);

    }
}
