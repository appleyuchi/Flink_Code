import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
//import com.alibaba.fastjson.serializer.SerializerFeature;


/*

代码来自:
https://www.cnblogs.com/myseries/p/10574184.html
 */
//遍历Json中的List

public class Traverse_List {

    public static void main(String[] args) throws FileNotFoundException, IOException {

//        TestClassLoader.class.getClassLoader();
        ClassLoader cl = Traverse_List.class.getClassLoader();
        InputStream inputStream = cl.getResourceAsStream("data1.json");


        System.out.print(inputStream);
        String jsontext = IOUtils.toString(inputStream, "utf8");

        JSONObject obj=JSONObject.parseObject(jsontext);//获取jsonobject对象
        JSONObject obj1 = obj.getJSONObject("data");//获取"data"这个key对应的value

        JSONArray jsonArray = obj1.getJSONArray("rows");//获取json中的列表[]
        System.out.println("jsonArray:"+jsonArray);

        JSONObject obj2 = jsonArray.getJSONObject(1);//对列表中具体元素进行选择
        System.out.println("obj2:" +obj2);

        for(Iterator iterator = jsonArray.iterator(); iterator.hasNext();)
        {
            JSONObject jsonObject1 = (JSONObject) iterator.next();
            System.out.println(jsonObject1);
        }

        info_util iu = JSON.parseObject(jsontext, info_util.class);//取得第一层JSONObject
        info_data_util du = JSON.parseObject(iu.getData(), info_data_util.class);//取得第二层JSONObject
        List<info_array_Util> olist = JSON.parseArray(du.getRows(), info_array_Util.class);//取得第三层JSONArray
        System.out.println(iu);
        System.out.println(du);
        System.out.println(olist);

    }

}
