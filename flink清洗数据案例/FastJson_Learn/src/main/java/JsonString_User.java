import com.alibaba.fastjson.JSON;
import java.io.FileNotFoundException;
import java.io.IOException;

public class JsonString_User {
    public static void main(String[] args) throws FileNotFoundException, IOException {
    //---------------------------json字符串 -> User------------------------
    /*
     * 字符串：{"name":"userGroup","users":[{"password":"123123"
     * ,"username":"zhangsan"},{"password":"321321","username":"lisi"}]}
     */
    String jsonStr3 = "{'name':'userGroup','users':[{'password':'123123','username':'zhangsan'},{'password':'321321','username':'lisi'}]}";
    UserGroup userGroup = JSON.parseObject(jsonStr3, UserGroup.class);
    System.out.println("json字符串转复杂java对象:" + userGroup);
}

}
