import com.alibaba.fastjson.JSON;

import java.io.FileNotFoundException;
import java.io.IOException;

public class String_User {
    public static void main(String[] args) throws FileNotFoundException, IOException {

//---------------------------json字符串 -> User------------------------
    /*
     *
     * 字符串：{"password":"123456","username":"dmego"}
     */

        String jsonStr1 = "{'password':'123456','username':'dmego'}";
        User user = JSON.parseObject(jsonStr1, User.class);
        System.out.println("json字符串转简单java对象:" + user.toString());
}


}
