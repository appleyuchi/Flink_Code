
|功能|备注|
|---|---|
|[字符串->User](src/main/java/Json_String_List_User.java)|列表形式
|[字符串->UserGroup](src/main/java/JsonString_User.java)|
|[遍历Json中的List](src/main/java/Traverse_List.java)||
|[User->json](src/main/java/User_json.java)||
|[字符串->Json->User](src/main/java/String_User.java)|key:value形式
|[对象列表转化为Json](src/main/java/List_User_json.java)||

FastJson认可的Json对象类型是JSONObject


实验来自
[Json详解以及fastjson使用教程](https://www.cnblogs.com/myseries/p/10574184.html)