import org.apache.commons.io.IOUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class InputStream_OutputStream
{
    public static void main(String[] args) throws IOException
    {
        InputStream is = IOUtils.toInputStream("This is a String", "utf-8");

        OutputStream os = new FileOutputStream("test2.txt");

        int bytes = IOUtils.copy(is, os);//返回长度
        System.out.println("File Written with " + bytes + " bytes");
        System.out.println("os:"+os);
        IOUtils.closeQuietly(os);


    }
}
