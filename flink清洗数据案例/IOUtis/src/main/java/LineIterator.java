import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class LineIterator
{

    public static void main(String[] args) throws FileNotFoundException, IOException
    {

        try(FileInputStream fin=new FileInputStream("test4.txt")){
            org.apache.commons.io.LineIterator lt= IOUtils.lineIterator(fin,"utf-8");
            while (lt.hasNext()){
                String line=lt.nextLine();
                System.out.println(line);
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }


}
