这里总共有3个文件夹<br>
①src只是一些log设置而已<br>
②DataClean清洗数据<br>
③DataReport数据汇报<br>


|文件|作用|调用/被调用关系|
|---|---|---|
|[DataReport.java](DataReport/src/main/java/henry/flink/DataReport.java)|读取Kafka topic中的auditLog并且写入ES(java版本)|被调用
|[kafkaProducer.java](DataClean/src/main/java/henry/flink/utils/kafkaProducer.java)|产生随机数据，并写入到topic-allData中|清洗阶段-数据源头
|[MyWatermark.java](DataReport/src/main/java/henry/flink/watermark/MyWatermark.java)|水位线|高铁票
|[kafkaProducerDataReport.java](DataReport/src/main/java/henry/flink/utils/kafkaProducerDataReport.java)|生成随机数据，传入Kafka的topic auditLog|报表阶段-数据源头
|[MyAggFunction.java](DataReport/src/main/java/henry/flink/function/MyAggFunction.java)||被调用
|[DataClean.java](DataClean/src/main/java/henry/flink/DataClean.java)|清洗來自topic-AllData和Redis的數據生成topic-allDataClean(java版本)|顶层文件
|[MyRedisSource.java](DataClean/src/main/java/henry/flink/customSource/MyRedisSource.java)|連接redis(java版本)|被调用

这个实验特别需要注意的地方，不要使用1.12的Flink
因为读写Kafka的适配于1.12的Flink-Kafka驱动还没有开发出来。


|主題|備註|
|---|---|
|allData|Kafka生產者對應的topic
|allDataClean|存放清洗后数据的topic
|auditLog|用来报表展示的数据
|LateLog|存储迟到的数据


博客里面的这个实验并没有把两个实验的结果进行衔接起来，
当然，这也是一种很好的调试习惯．

实验分如下:

<h3>①清洗阶段(完成)</h3>
````
service firewalld stop
````
````
运行KafkaProducer.java生成隨機數據
````
````
$KAFKA/bin/kafka-console-consumer.sh --bootstrap-server Desktop:9091 --from-beginning --topic allData
查看是否顺利生成随机数据
````
````
运行DataClean.java来清洗数据
````
````
$KAFKA/bin/kafka-console-consumer.sh --bootstrap-server Desktop:9091 --from-beginning --topic allDataClean
查看是否顺利清洗数据并写入到kafka中
````

其他信息请参考博客:<br>
[Flink数据清洗(Kafka事实表+Redis维度表)](https://yuchi.blog.csdn.net/article/details/112308759)



清洗效果

|数据|举例|
|---|---|
|原始数据||
|清洗结果|{"dt":"2021-01-11 13:57:07","score":0.2,"level":"D","type":"s1"}|


<h3>②报表阶段(完成)</h3>

````
运行kafkaProducerDataReport生成隨機數據
````
````
$KAFKA/bin/kafka-console-consumer.sh --bootstrap-server Desktop:9091 --from-beginning --topic auditLog
查看是否顺利生成随机数据
````

````
./kibana.sh在ElasticSearch中生成用来接收数据的"数据库/表/字段"
````

````
运行DataReport.java把数据从kafka传输到ElasticSearch
````
````
$KAFKA/bin/kafka-console-consumer.sh --bootstrap-server Desktop:9091 --from-beginning --topic lateLog
查看是否收集迟到的数据
````
其他信息参考博客:
[kafka-＞Flink-＞ElasticSearch(Java形式)](https://yuchi.blog.csdn.net/article/details/112500258)
