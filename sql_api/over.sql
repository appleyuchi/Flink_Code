---mysql的flink-test中建立下面表格
create database `flink-test`;
use flink-test;
create table tmall_item
(
    itemID varchar(32) not null,
    itemType varchar(32) not null,
    eventtime TIMESTAMP(3) not null,
    price Double not null
);

---修改列名
alter table tmall_item change  column onSellTime eventtime varchar(32);
---mysql中执行下面的语句
INSERT INTO tmall_item (itemID,itemType,eventtime,price) VALUES("ITEM001","Electronic","2017-11-11 10:01:00",20);
INSERT INTO tmall_item (itemID,itemType,eventtime,price) VALUES("ITEM002","Electronic","2017-11-11 10:02:00",50);
INSERT INTO tmall_item (itemID,itemType,eventtime,price) VALUES("ITEM003","Electronic","2017-11-11 10:03:00",30);
INSERT INTO tmall_item (itemID,itemType,eventtime,price) VALUES("ITEM004","Electronic","2017-11-11 10:03:00",60);
INSERT INTO tmall_item (itemID,itemType,eventtime,price) VALUES("ITEM005","Electronic","2017-11-11 10:05:00",40);
INSERT INTO tmall_item (itemID,itemType,eventtime,price) VALUES("ITEM006","Electronic","2017-11-11 10:06:00",20);
INSERT INTO tmall_item (itemID,itemType,eventtime,price) VALUES("ITEM007","Electronic","2017-11-11 10:07:00",70);
INSERT INTO tmall_item (itemID,itemType,eventtime,price) VALUES("ITEM008","Clothes"   ,"2017-11-11 10:08:00",20);






---flink sql client中执行下面的语句
CREATE TEMPORARY TABLE tmall_item(
   itemID VARCHAR,
   itemType VARCHAR,
   eventtime VARCHAR,
   onSellTime AS TO_TIMESTAMP(eventtime),
   price DOUBLE,
   WATERMARK FOR onSellTime AS onSellTime - INTERVAL '1' SECONDS) WITH 
(
    'connector.type'                ='jdbc',
    'connector.url'                 ='jdbc:mysql://Desktop:3306/flink-test',
    'connector.table'               ='tmall_item',
    'connector.username'            ='appleyuchi',
    'connector.password'            ='appleyuchi',
    'connector.write.flush.max-rows'='1'
);

---Bounded ROWS OVER Window场景示例
SELECT
    itemID,
    itemType,
    onSellTime,
    price,  
    MAX(price) OVER (
        PARTITION BY itemType 
        ORDER BY onSellTime 
        ROWS BETWEEN 2 preceding AND CURRENT ROW) AS maxPrice
  FROM tmall_item;

  -- 最大值另外取名为maxPrice


---Bounded RANGE OVER Window场景示例
SELECT  
    itemID,
    itemType, 
    onSellTime, 
    price,  
    MAX(price) OVER (
        PARTITION BY itemType 
        ORDER BY onSellTime 
        RANGE BETWEEN INTERVAL '2' MINUTE preceding AND CURRENT ROW) AS maxPrice
  FROM tmall_item;     