---下面是单分区表
create table t1(
id int,
name string,
likes array<string>,
adress map<string,string>
)
partitioned by (department string)
row format delimited
fields terminated by ','
collection items terminated by '-'
map keys terminated by ':'
lines terminated by '\n';


load data local inpath '/home/appleyuchi/桌面/hive动态静态分区/test.txt' into table t1 partition(department='depart-1');
load data local inpath '/home/appleyuchi/桌面/hive动态静态分区/test.txt' into table t1 partition(department='depart-2');

0: jdbc:hive2://Desktop:10000> show partitions t1;
+----------------------+
|      partition       |
+----------------------+
| department=depart-1  |
| department=depart-2  |
+----------------------+




---下面是多分区表
create table t2(
id int,
name string,
likes array<string>,
adress map<string,string>
)
partitioned by (department string,sex string)
row format delimited
fields terminated by ','
collection items terminated by '-'
map keys terminated by ':'
lines terminated by '\n';


load data local inpath '/home/appleyuchi/桌面/hive动态静态分区/test.txt' into table t2 partition(department='GBD',sex='M');
load data local inpath '/home/appleyuchi/桌面/hive动态静态分区/test.txt' into table t2 partition(department='GBD',sex='F');
load data local inpath '/home/appleyuchi/桌面/hive动态静态分区/test.txt' into table t2 partition(department='ISD',sex='M');
load data local inpath '/home/appleyuchi/桌面/hive动态静态分区/test.txt' into table t2 partition(department='ISD',sex='F');


---下面是动态分区实验
create table t3(
id int,
name string,
age int,
gender string,
likes array<string>,
address map<string,string>
)
row format delimited
fields terminated by ','
collection items terminated by '-'
map keys terminated by ':'
lines terminated by '\n';

load data local inpath '/home/appleyuchi/桌面/hive动态静态分区/test2.txt' into table t3;

create table t4(
id int,
name string,
likes array<string>,
address map<string,string>
)
partitioned by (age int,gender string)
row format delimited
fields terminated by ','
collection items terminated by '-'
map keys terminated by ':'
lines terminated by '\n';



set hive.exec.dynamic.partition=true; //开启动态分区功能（默认true，开启）
set hive.exec.dynamic.partition.mode=nonstrict;   //设置为非严格模式（动态分区的模式，默认strict，表示必须指定至少一个分区为静态分区，nonstrict模式表示允许所有的分区字段都可以使用动态分区。）
insert into table t4 partition(age,gender) select id,name,likes,address,age,gender from t3;

这里注意:
因为t3比t4多了2个字段,
所以t4会以多出来的这两个字段进行动态分区(也就是HDFS上的多级目录结构)




---下面是动静结合的使用方法
create table t5(id int) partitioned by (country string,city string);

create table t0(
id int,
city string
)
row format delimited
fields terminated by ','
collection items terminated by '-'
map keys terminated by ':'
lines terminated by '\n';

load data local inpath '/home/appleyuchi/桌面/hive动态静态分区/test3.txt' into table t0;


set hive.exec.dynamic.partition=true;
set hive.exec.dynamic.partition.mode=nonstrict;
insert overwrite table t5 partition(country='china',city) select id ,city from t0;
#country分区为静态，city为动态分区，以查询的city字段为分区名