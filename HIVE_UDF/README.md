|Hive的UDF(代码)|博文链接|
|---|---|
|[GenericUDAF](../FLINK读写各种数据源/Java/src/main/java/FieldLength.java) |[GenericUDAF使用流程记载](https://yuchi.blog.csdn.net/article/details/112068144)|
|[GenericUDTF](src/main/java/FeatureParseUDTF.java)|[GenericUDTF使用流程记载](https://yuchi.blog.csdn.net/article/details/112071373) |
|[GenericUDF](../FLINK读写各种数据源/Java/src/main/java/helloGenericUDFNew.java)  |[GenericUDF使用流程记载](https://yuchi.blog.csdn.net/article/details/112075116) |
|[UDAF](../FLINK读写各种数据源/Java/src/main/java/UDAFSum_Sample.java)        |[hive中的UDAF的使用流程记载](https://yuchi.blog.csdn.net/article/details/112117888)|
|UDTF|没找到相关案例<br>应该是已经被官方废弃|
|[UDF](src/main/java/HelloUdf.java) |[hive臨時udf與永久udf詳細操作流程](https://blog.csdn.net/appleyuchi/article/details/111358500)|


