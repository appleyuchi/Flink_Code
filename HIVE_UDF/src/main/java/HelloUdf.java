import org.apache.hadoop.hive.ql.exec.UDF;

public class HelloUdf extends UDF {
    public String evaluate (String input){
        return "Hello:"+input;
    }
}