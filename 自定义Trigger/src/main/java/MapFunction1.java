import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;

public class MapFunction1 implements MapFunction<String,Tuple2<String, Integer>>
{
    public Tuple2<String, Integer> map(String value) throws Exception
    {
        try
        {
            JSONObject jsonObject = JSON.parseObject(value);
            String itemId = jsonObject.getString("itemId");//获取value
            return new Tuple2<String, Integer>(itemId, 1);
        } catch (Exception e) {
            return Tuple2.of(null, null);
        }
    }
}
