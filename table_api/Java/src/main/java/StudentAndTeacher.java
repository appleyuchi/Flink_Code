
import lombok.Data;
import java.sql.Timestamp;

@Data
public class StudentAndTeacher {
    private String name;
    private String sex;
    private String course;
    private Float score;
    private Timestamp sysDate;
    private String teacherName;

}