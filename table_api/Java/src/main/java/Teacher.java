
import lombok.Data;

@Data
public class Teacher {

    private String teacherName;
    private String studentName;

}