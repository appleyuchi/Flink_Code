import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;

import static org.apache.flink.table.api.Expressions.$;
import java.util.Arrays;

public class Filter
{


    public static void main(String[] args) throws Exception
    {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        StreamTableEnvironment tEnv = StreamTableEnvironment.create(env);

        DataStream<Order> orderA = env.fromCollection(Arrays.asList(
                new Order(1L, "beer", 3),
                new Order(3L, "rubber", 2),
                new Order(1L, "diaper", 4)
        ));

        tEnv.createTemporaryView("Orders", orderA);

        Table orders = tEnv.from("Orders");
        Table result2 = orders.filter($("amount").mod(2).isEqual(0));
//        如果amount对2去取模，得到余数0就删除掉，保留剩下的．
        tEnv.toAppendStream(result2, Row.class).print();
        env.execute();
    }


}
