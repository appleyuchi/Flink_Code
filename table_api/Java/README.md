Intellij运行需要导入一个依赖包:
Project Structure->Global Libraries->导入flink-table_2.11-1.12-SNAPSHOT.jar






[下面表格中已经包含的官方代码实例](https://github.com/apache/flink/tree/master/flink-examples/flink-examples-table/src/main/java/org/apache/flink/table/examples/java/basics)<br>

|实验|备注(相关说明链接)|
|---|---|
|[官方入门例子](src/main/java/GettingStartedExample.java)||
|[官方例子-WordCountTable](src/main/java/WordCountTable.java)||
|[官方例子-WordCountSQL](src/main/java/WordCountSQL.java)||
|[官方例子-StreamSQL](src/main/java/StreamSQLExample.java)||
|[官方例子-StreamWindowSQL](src/main/java/StreamWindowSQLExample.java)|处理带有时间戳的数据|
|[自动生成测试类](src/main/java/springServiceImpl.java)|[Intellij自动生成测试类](https://yuchi.blog.csdn.net/article/details/109122193)|
|[UDF使用](src/main/java/UDF.java)|一個UDF使用的例子|
|[flink通过sql查询数据](src/main/java/TableSQLDemo.java)|[flink通过sql查询数据的代码实现](https://blog.csdn.net/nengyu/article/details/97686030)|
|[UDF第二个例子](src/main/java/UDF2.java)|[Flink Sql教程（4）](https://www.jianshu.com/p/ee8b90048de4)|
|[UDTF](src/main/java/UDTF.java)|[Flink Sql教程（4）](https://www.jianshu.com/p/ee8b90048de4)|
|[UDAF](src/main/java/UDAF.java)|[Flink Sql教程（4）](https://www.jianshu.com/p/ee8b90048de4)|
|[UDTAF](src/main/java/UDTAF.java)|[Flink Sql教程（4）](https://www.jianshu.com/p/ee8b90048de4)|


上面表格中的例子可能會出現重複。




[官方文档-Table API(需要当前重点解决)](https://ci.apache.org/projects/flink/flink-docs-stable/dev/table/tableApi.html)<br>
Joins操作需要的pojo来自：<br>
[Mysql inner join on的用法实例](https://www.jb51.net/article/109966.htm)


|算子|说明|
|---|---|
|[Overview & Examples](src/main/java/Overview_Examples.java)|只包含官方文档中的第一个例子，不包含第二个例子(太乱了)|
|[Table/SQL各种语法汇总](src/main/java/SyntaxTry.java)|[flink的table/sql api的多种写法汇总](https://yuchi.blog.csdn.net/article/details/109184189)|
|[From](src/main/java/From.java)|从xx表格
|[Values](src/main/java/Values.java)|读取一行
|[Select](src/main/java/Select.java)|Select xxx from一个表格
|[As](src/main/java/As.java)|列名重命名
|[Where](src/main/java/Where.java)|附加条件
|[Filter](src/main/java/Filter.java)|过滤
|[AddColumns](src/main/java/AddColumns.java)|某个字段的数据拼接新的字符串<br>然后形成新的一列
|[AddOrReplaceColumns](src/main/java/AddOrReplaceColumns.java)|新增/替换某列
|[DropColumns](src/main/java/DropColumns.java)|删除某列
|[RenameColumns](src/main/java/RenameColumns.java)|重命名某个字段
|[GroupBy Aggregation](src/main/java/GroupByAggregation.java)|groupy结果进行聚合
|[GroupBy Window Aggregation](src/main/java/GroupByWindowAggregation.java)|完成
|[Over Window Aggregation](src/main/java/OverWindowAggregation.java)|完成
|[Distinct Aggregation-1](src/main/java/DistinctAggregation1.java)<br>[Distinct Aggregation-2](src/main/java/DistinctAggregation2.java)<br>[Distinct Aggregation-3](src/main/java/DistinctAggregation3.java)<br>[Distinct Aggregation-4](src/main/java/DistinctAggregation4.java)|3在官方确认是1.12的bug<br>（已經mailing list）
|[Distinct](src/main/java/Distinct.java)|去重计算
|[Inner Join](src/main/java/InnerJoin.java)|兩個表各自指定的一列，相等的數據做join操作
|[Outer Join](src/main/java/OuterJoin.java)|左外聯，右外聯
|[Interval Join](src/main/java/SimpleTimeIntervalJoin.java)|订单数据和延时付款数据的Join操作:<br>[两条水位线的业务需求分析-Interval JOIN方案](https://yuchi.blog.csdn.net/article/details/111142249)
|[Inner Join with Table Function (UDTF)](src/main/java/InnerJoinwithTableFunction.java)|Table和UDTF的function返回結果做join操作
|[Left Outer Join with Table Function (UDTF)](src/main/java/LeftOuterJoinwithTableFunction.java)|Table和UDTF的function返回結果做LeftJoin操作
|[Join with Temporal Table(rowtime)](src/main/java/JoinwithTemporalTable_rowtime.java)<br>[Join with Temporal Table(proctime)](src/main/java/JoinwithTemporalTable_proctime.java)|TemporalTableFunction和Table做Join操作
|[Union](src/main/java/Union.java)|自动压缩多个结果集合中的重复结果(自动去重，属于集合概念)
|[UnionAll](src/main/java/UnionAll.java)|将所有的结果全部显示出来,不管是不是重复(不去重，不属于集合概念)
|[Intersect](src/main/java/Intersect.java)|交集(结果去重)
|[IntersectAll](src/main/java/IntersectAll.java)|交集(结果不去重)
|[Minus](src/main/java/Minus.java)|差集运算(结果去重)
|[MinusAll](src/main/java/MinusAll.java)|差集运算(结果不去重)
|[In](src/main/java/In.java)|Source1的某列在Source2中
|[Order By](src/main/java/OrderBy.java)|根據某列排序
|[Offset & Fetch](src/main/java/Offset_Fetch.java)|offset的意思是:<br>从第几条数据开始处理，前面的忽略
|[Insert Into](src/main/java/InsertInto.java)|如果忘记写pom依赖的话,会无法写入mysql
|Group Windows:<br>①[Tumble (Tumbling Windows)](src/main/java/TumblingWindows.java)<br>②[Slide (Sliding Windows)](Java/src/main/java/SlidingWindows.java)<br>③[Session (Session Windows)](src/main/java/SessionWindows.java)
|Over Windows:<br>①[Unbounded Over Windows](src/main/java/UnboundedOverWindows.java)<br>②[Bounded Over Windows](Java/src/main/java/BoundedOverWindows.java)
|[Map](src/main/java/Map.java)|映射
|[FlatMap](src/main/java/FlatMap.java)|打散后映射
|[Aggregate](src/main/java/Aggregate.java)|聚合
|[Group Window Aggregate](src/main/java/GroupWindowAggregate.java)|
|[FlatAggregate(目前不支持Batch)](src/main/java/FlatAggregate.java)|打散后聚合
|[Group Window FlatAggregate](src/main/java/GroupWindowFlatAggregate.java)|


暫時不看的文檔<br>

[flink sql的数据类型和Java/Scala的对照关系](https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/table/types.html)<br>
[Flink SQL客戶端的使用](https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/table/sqlClient.html)<br>
[基本配置](https://ci.apache.org/projects/flink/flink-docs-stable/dev/table/)

已经阅读的文档:<br>
①[Table-common(里面的绝大多数用法都在上面的example里面涉及到了)](https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/table/common.html)<br>
②[TableApI](https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/table/tableApi.html)(与上面的一个重复)<br>
③[EXPLAIN Statements](https://ci.apache.org/projects/flink/flink-docs-stable/dev/table/sql/explain.html)<br>
④[和其他開源組件的連接方式，以及一些連接驅動jar](https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/table/connect.html)<br>
⑤[Time Attributes](https://ci.apache.org/projects/flink/flink-docs-release-1.12/dev/table/streaming/time_attributes.html)

[暂时不包含的官方案例(尚未搞清楚用途)](https://github.com/apache/flink/tree/master/flink-examples/flink-examples-table/src/main/java/org/apache/flink/table/examples/java/connectors)

