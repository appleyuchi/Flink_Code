import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment}

object RemoteSubmitApp extends App
{

  val host: String = "Desktop"
  val port: Int = 8085
  val jarFiles = "/home/appleyuchi/桌面/Flink_Code/flink_state/target/datastream_api-1.0-SNAPSHOT.jar"

  val env = StreamExecutionEnvironment.createRemoteEnvironment(host, port, jarFiles)

  val socketHost: String = "Desktop"
  val socketPort: Int = 9999
  val socketDs: DataStream[String] = env.socketTextStream(socketHost, socketPort)

  socketDs.print()

//  socketDs.flatMap(_.split(" "))
//    .map((_, 1))
//    .keyBy(0)
//    .sum(1)
//    .print()

  env.execute("Remote Submit Job")
}


//实验运行结果在/home/appleyuchi/bigdata/flink-1.12/log中的.out文件中

//代码来自：
//https://www.jianshu.com/p/1e45cf925569
