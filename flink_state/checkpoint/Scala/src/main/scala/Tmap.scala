import org.apache.flink.api.common.functions.MapFunction

/**当接收的数据是error字符串时候，抛出异常*/
class TMap() extends MapFunction[String, String] {
  override def map(t: String): String = {
    if ("error".equals(t)) {
      throw new RuntimeException("error message ")
    }
    t
  }
}