Flink的state实验

|代码|说明|
|---|---|
|[RemoteSubmitApp.scala](./flink_state/savepoint/src/main/scala/RemoteSubmitApp.scala)|Intellij远程连接Flink实验|
|[savepoint实验(java)](./flink_state/savepoint/Java)|[实验说明](https://yuchi.blog.csdn.net/article/details/108865441)|
|[savepoint实验(scala)](./flink_state/savepoint/Scala)|[实验说明](https://yuchi.blog.csdn.net/article/details/108877699)|
|[checkpoint实验(scala)](./flink_state/checkpoint/Scala/)|[实验说明](https://yuchi.blog.csdn.net/article/details/108882948)|
|[增量checkpoint实验(scala)](./flink_state/checkpoint/Scala/src/main/scala/wordcount_increstate.scala)|[实验说明](https://yuchi.blog.csdn.net/article/details/108896441)
||二阶提交kafka->Flink->Mysql(exactly-once)|
|[Query State实验](./flink_state/Query_state/src/main/java)|失败(目前无法跑通)<br>[Query State Beta](https://ci.apache.org/projects/flink/flink-docs-stable/dev/stream/state/queryable_state.html)|
|[ValueStateDescriptor用法举例](state_use/src/main/java/WordCountKeyedState.java)|完成|
|[MapStateDescriptor用法举例](state_use/src/main/java/StateExampleJ.java)|完成|
|[ContinuousProcessingTimeTrigger的用法](state_use/src/main/java/ContinueTriggerDemo.java)|①nc -lk 8001<br>②运行ContinueTriggerDemo<br>③aa<br>aa<br>bb



官方文档有:<br>
[1][Working with State](https://ci.apache.org/projects/flink/flink-docs-stable/dev/stream/state/state.html)<br>
[2][State Processor API](https://ci.apache.org/projects/flink/flink-docs-stable/dev/libs/state_processor_api.html)<br>
[3][State & Fault Tolerance](https://ci.apache.org/projects/flink/flink-docs-stable/dev/stream/state/)<br>
[4][State Backends](https://ci.apache.org/projects/flink/flink-docs-stable/ops/state/state_backends.html)<br>