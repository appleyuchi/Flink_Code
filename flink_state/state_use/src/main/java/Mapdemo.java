import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class Mapdemo implements MapFunction<Tuple2<String, Integer>, Tuple3<String, String, Integer>>
{

    @Override
    public Tuple3<String, String, Integer> map(Tuple2<String, Integer> value) throws Exception {
        // TODO Auto-generated method stub

        DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String s = format2.format(new Date());

        return new Tuple3<String, String, Integer>(value.f0, s, value.f1);
    }
}