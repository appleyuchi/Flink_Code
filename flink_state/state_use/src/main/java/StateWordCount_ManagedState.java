import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.contrib.streaming.state.RocksDBStateBackend;
import org.apache.flink.runtime.state.StateBackend;
import org.apache.flink.runtime.state.filesystem.FsStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

import java.nio.file.Path;

public class StateWordCount_ManagedState
{

    public static void main(String[] args) throws Exception
    {

        final ParameterTool parameters = ParameterTool.fromArgs(args);
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.getConfig().setGlobalJobParameters(parameters);

        // Checkpoint
        env.enableCheckpointing(5000, CheckpointingMode.EXACTLY_ONCE);

        // StateBackend
        StateBackend stateBackend = new RocksDBStateBackend("hdfs://Desktop:9000/flink/checkpoints",true);
//        這個FsStateBackend需要設置爲一個文件夾

        env.setStateBackend(stateBackend);

        env.addSource(new SourceFromFile())
                .setParallelism(1)
                .name("demo-source")
                .flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>()
                {
                    @Override
//                    輸入的是String value
//                    輸出的是Collector<Tuple2<String,Integer>> out
                    public void flatMap(String value, Collector<Tuple2<String, Integer>> out) throws Exception {
                        String[] arr = value.split(",");
                        for (String item : arr) {
                            out.collect(new Tuple2<>(item, 1));
                        }
                    }
                })
                .name("demo-flatMap")
                .keyBy(0)
                .flatMap(new WordCountFlatMap())
                .print();

        env.execute("StateWordCount");
    }

}