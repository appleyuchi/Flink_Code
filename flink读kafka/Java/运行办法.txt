代码主要由两部分组成：
MessageSplitter类、MessageWaterEmitter类、MKafkaMessageStreaming类：
Flink streaming实时处理Kafka消息类

KafkaProducerTest类、MMemoryUsageExtrator类：
构建Kafka测试消息

运行办法:
①启动Hadoop、Zookeeper(在各个节点分别启动)、Flink、Kafka(在各个节点分别启动)集群
②运行KafkaProducerTest.java
③运行KafkaMessageStreaming.java
④重启Desktop中的kafka节点
⑤回到Intellij，可以看到KafkaMessageStreaming.java的窗口输出了被消费的数据

----------------------------------------------------排查方案----------------------------------------------------
查看kafka集群中的topic:
$KAFKA/bin/kafka-topics.sh --list --zookeeper Desktop:2181

如果Flink读取不到数据，可以使用kafka命令进行消费:
cd $KAFKA/bin
./kafka-console-consumer.sh --bootstrap-server Desktop:9091,Laptop:9092,Laptop:9093 --topic test-0921