public class person
{
    public String name;
    public int value;
    public int weight;

    public person()
    {
    }

    public person(String name,int value,int weight)
    {
        this.name = name;
        this.value = value;
        this.weight = weight;
    }

    @Override
    public String toString()
    {
        return "person{" +
                "name=" + name +
                ",value='" + value + '\'' +
                ",weight=" + weight +
                '}';
    }
}