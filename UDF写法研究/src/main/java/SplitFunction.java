import org.apache.flink.table.annotation.DataTypeHint;
import org.apache.flink.table.annotation.FunctionHint;
import org.apache.flink.table.functions.TableFunction;
import org.apache.flink.types.Row;

//這個是UDTF的例子
@FunctionHint(output = @DataTypeHint("ROW<word STRING, length INT>"))
public class SplitFunction extends TableFunction<Row>
{

    public void eval(String str)
    {
        for (String s : str.split("#"))//这个地方不用紧张，就是分隔符
        {
            // use collect(...) to emit a row
            collect(Row.of(s, s.length()));
        }
    }
}




/*
下面的是java的for循环

for(Integer in : integers)
{
    System.out.println(in);
}
 */