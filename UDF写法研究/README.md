|文档目录(代码)|备注|
|---|---|
|[Scalar Functions](src/main/java/ScalarFunctions.java)|UDF|
|[Table Functions](src/main/java/TableFunctions.java)|UDTF
|[Aggregate Functions](src/main/java/AggregateFunctions.java)|UDAF
|[Table Aggregate Functions](src/main/java/TableAggregateFunctions.java)|UDTAF



Reference:<br>
[User-defined Functions](https://ci.apache.org/projects/flink/flink-docs-release-1.12/dev/table/functions/udfs.html#aggregate-functions)
