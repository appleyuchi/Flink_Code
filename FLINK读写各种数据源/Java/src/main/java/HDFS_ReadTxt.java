import lombok.val;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.core.fs.FileSystem;


public class HDFS_ReadTxt {

    public static void main(String[] args) throws Exception
    {
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        String inPath = "hdfs://Desktop:9000/sales.csv";
        String outPath= "hdfs://Desktop:9000/test004.txt";

        DataSet<String> lines = env.readTextFile("hdfs://Desktop:9000/test001.txt")
                .name("HDFS File read");
        lines.print();
        ((DataSource<String>) lines).setParallelism(1).writeAsText(outPath,FileSystem.WriteMode.OVERWRITE);



        env.execute();
    }
}

