import lombok.Data;

@Data
public class OrderCost {

    private String parentOrderNo;

    private String memberId;

    private String unionId;

//    private String extraCost;

    private String createTime;
}
