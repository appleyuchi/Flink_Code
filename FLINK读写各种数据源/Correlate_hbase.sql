hbase shell
create 't2',{NAME => 'f1'}
put 't2','1589186680','f1:col1','2020-05-11 16:44:40'
scan 't2'

CREATE TABLE t1 ( 
afterColumns ROW(created STRING,extra ROW(canGiving BOOLEAN),`parameter` ARRAY <INT>) ,
beforeColumns STRING ,
tableVersion ROW(binlogFile STRING,binlogPosition INT ,version INT) ,
p AS PROCTIME(),
touchTime bigint 
) WITH (
    'connector.type' = 'kafka',  -- 指定连接类型是kafka
    'connector.version' = 'universal',  -- 与我们之前Docker安装的kafka版本要一致
    'connector.topic' = 'json_parse', -- 之前创建的topic 
    'connector.properties.group.id' = 'flink-test-0', -- 消费者组，相关概念可自行百度
    'connector.startup-mode' = 'earliest-offset',  --指定从最早消费
    'connector.properties.zookeeper.connect' = 'Desktop:2181',  -- zk地址
    'connector.properties.bootstrap.servers' = 'Desktop:9091',  -- broker地址
    'format.type' = 'json'  -- json格式，和topic中的消息格式保持一致
);

select * from t1;





CREATE TABLE t2 (
rowkey String,
f1 ROW<col1 String>
) WITH (
  'connector' = 'hbase-2.2',          -- required: valid connector versions are "1.4.3"
  'table-name' = 't2',  -- required: hbase table name
  'zookeeper.quorum' = 'Desktop:2181', -- required: HBase Zookeeper quorum configuration
  'zookeeper.znode.parent' = '/hbase'    -- optional: the root dir in Zookeeper for HBase cluster.
);


select * from t2;


CREATE TABLE hTable (
 rowkey INT,
 family1 ROW<q1 INT>,
 family2 ROW<q2 STRING, q3 BIGINT>,
 family3 ROW<q4 DOUBLE, q5 BOOLEAN, q6 STRING>,
 PRIMARY KEY (rowkey) NOT ENFORCED
) WITH (
 'connector' = 'hbase-1.4',
 'table-name' = 'mytable',
 'zookeeper.quorum' = 'localhost:2181'
);



select a.* ,b.* from t1 a left join  t2 FOR SYSTEM_TIME AS OF a.p AS b on a.afterColumns.created = b.rowkey;