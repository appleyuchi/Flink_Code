CREATE TABLE t1 ( 
    afterColumns ROW(created STRING,extra ROW(canGiving BOOLEAN),`parameter` ARRAY <INT>) ,
    beforeColumns STRING ,
    tableVersion ROW(binlogFile STRING,binlogPosition INT ,version INT) ,
    touchTime bigint 
    ) WITH (
        'connector.type' = 'kafka',  -- 指定连接类型是kafka
        'connector.version' = 'universal',  -- 与我们之前Docker安装的kafka版本要一致
        'connector.topic' = 'json_parse', -- 之前创建的topic 
        'connector.properties.group.id' = 'flink-test-0', -- 消费者组，相关概念可自行百度
        'connector.startup-mode' = 'earliest-offset',  --指定从最早消费
        'connector.properties.zookeeper.connect' = 'Desktop:2181',  -- zk地址
        'connector.properties.bootstrap.servers' = 'Desktop:9091',  -- broker地址
        'format.type' = 'json'  -- json格式，和topic中的消息格式保持一致
    );



Flink SQL> select * from t1;
[INFO] Result retrieval cancelled.

Flink SQL> select  afterColumns.extra.canGiving from t1;
[INFO] Result retrieval cancelled.

Flink SQL> select afterColumns.`parameter`[1] from t1;
[INFO] Result retrieval cancelled.
