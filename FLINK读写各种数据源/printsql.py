a="CREATE external TABLE fs_table (\n" \
                         "  user_id STRING,\n" \
                         "  order_amount DOUBLE" \
                         ") partitioned by (dt string,h string,m string) " \
                         "stored as ORC " \
                         "TBLPROPERTIES (\n" \
                         "  'partition.time-extractor.timestamp-pattern'='$dt $h:$m:00',\n" \
                         "  'sink.partition-commit.delay'='0s',\n" \
                         "  'sink.partition-commit.trigger'='partition-time',\n" \
                         "  'sink.partition-commit.policy.kind'='metastore'" \
                         ")"
print(a)