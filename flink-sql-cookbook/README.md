Apache Flink SQL Cookbook


Table of Contents(目录)
Foundations(基础内容)

|内容|备注|
|---|---|
|[Creating Tables(建立表格)](SQL/Creating_Tables.sql)|有错，生成的数据包含NULL
|[Inserting Into Tables(插入表格)](SQL/Inserting＿Into＿Tables．sql)|server_logs->client_errors
|[Working with Temporary Tables(临时表)](SQL/Working_with_Temporary_Tables.sql)|server_logs->client_errors
|[Filtering Data](SQL/Filtering_Data.sql)|过滤数据
|[Aggregating Data](SQL/Aggregating_Data.sql)|聚合数据
|[Sorting Tables](SQL/Sorting_Tables.sql)|表排序
|[Encapsulating Logic with (Temporary) Views](SQL/Encapsulating_Logic_with_Temporary_Views.sql)|建一个view,然后可以在这个view上面增删改查
|[Writing Results into Multiple Tables](SQL/Writing_Results_into_Multiple_Tables.sql)|结果写入多个表中<br>只適用於ververica平臺 <br>不适用于Flink SQL Client中运行

聚合：<br>根据某一统计方法，遍历所有数据，得出一个统计值

Aggregations and Analytics

|内容|备注|
|---|---|
|[Aggregating Time Series Data(聚合时序数据)](SQL/Aggregating_Time_Series_Data.sql)|Group By Window的使用
|[Watermarks(水位线)](SQL/Watermarks.sql)|完成
|[Analyzing Sessions in Time Series Data](SQL/Analyzing_Sessions_in_Time_Series_Data.sql)|[Flink SQL Client中的session window图解](https://yuchi.blog.csdn.net/article/details/113741340)
|[Rolling Aggregations on Time Series Data](SQL/Rolling_Aggregations_on_Time_Series_Data.sql)|[Flink SQL Client的Rolling Aggregation实验解析](https://yuchi.blog.csdn.net/article/details/113618603)
|[Continuous Top-N(统计几个数据)](SQL/Continuous_Top-N.sql)|连续聚合(前N项)
|[Deduplication](SQL/Deduplication.sql)|去重
|[Chained (Event) Time Windows](SQL/Chained_Windows.sql)|[Flink SQL Client中的Chained (Event) Time Windows](https://yuchi.blog.csdn.net/article/details/113644745)
|[Detecting Patterns with MATCH_RECOGNIZE(模式检测)](SQL/Detecting_Patterns_with_MATCH_RECOGNIZE.sql)|完成

Other Built-in Functions

|内容|备注|
|---|---|
|[Working with Dates and Timestamps](SQL/Working_with_Dates_and_Timestamps.md)|SQL中使用时间戳

Joins


|内容|备注|
|---|---|
|[Regular Joins(常规Join操作)](SQL/Regular_Joins.sql)|表NOC和表RealNames做Join操作
|[Interval Joins](SQL/Interval_Joins.sql)|[flink sql实现interval join的图解](https://yuchi.blog.csdn.net/article/details/113408536)
|[Temporal Table Join between a non-compacted and compacted Kafka Topic](SQL/Temporal_Table_Join_between_a_non-compacted_and_compacted_Kafka_Topic.sql)|[kafka中的compacted topics](https://yuchi.blog.csdn.net/article/details/113745669)
|[Lookup Joins](SQL/lookup_joins.sql)|和mysql中的數據做join操作
|[Star Schema Denormalization(N-Way Join)](SQL/N_Way_Join.sql)|[Flink SQL的N way join](https://yuchi.blog.csdn.net/article/details/113754291)
|[Lateral Table Join](SQL/lateral_join.sql)|根据Join条件遍历内外两张表格

关于上述的faker这个connector,相关依赖安装办法是：<br>
git clone https://gitee.com/fastsource/flink-faker<br>
mvn clean package<br>
然后把生成的flink-faker-0.1.1.jar放到$FLINK_HOME/lib下面


[SQL Client官方文档](https://ci.apache.org/projects/flink/flink-docs-stable/dev/table/sqlClient.html)