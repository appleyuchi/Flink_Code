# 09 Analyzing Sessions in Time Series Data

本例子:
在session window中聚合实时数据

 on Java Faker expressions.

#### What are Session Windows?

Session Windows　aggregate records into _sessions_ 
that represent periods of activity followed by gaps of idleness. 

Think, for example, of user sessions on a website: 
a user will be active for a given period of time, then leave the website; 
and each user will be active at different times. 

用户会在某段时间特别活跃，然后离开这个网站，
为了分析用户的这种行为，聚合他们在网站上的行为数据

#### Using Session Windows
计算每个用户在session window的持续时间（duration）内的请求响应是403的数据。

http请求在上一个请求之后10s内(包含)发送(请求)会被认为是同一个session内,
session之间的间隔不能超过10s(不包含10s
),否则会新建立一个session



## Script

CREATE TABLE server_logs ( 
    client_ip STRING,
    client_identity STRING, 
    userid STRING, 
    log_time TIMESTAMP(3),
    request_line STRING, 
    status_code STRING, 
    WATERMARK FOR log_time AS log_time - INTERVAL '5' SECONDS
) WITH (
  'connector' = 'faker', 
  'rows-per-second' = '5',
  'fields.client_ip.expression' = '#{Internet.publicIpV4Address}',
  'fields.client_identity.expression' =  '-',
  'fields.userid.expression' =  '#{regexify ''(morsapaes|knauf|sjwiesman){1}''}',
  'fields.log_time.expression' =  '#{date.past ''5'',''SECONDS''}',
  'fields.request_line.expression' = '#{regexify ''(GET|POST|PUT|PATCH){1}''} #{regexify ''(/search\.html|/login\.html|/prod\.html|cart\.html|/order\.html){1}''} #{regexify ''(HTTP/1\.1|HTTP/2|/HTTP/1\.0){1}''}',
  'fields.status_code.expression' = '#{regexify ''(200|201|204|400|401|403|301){1}''}'
);



SELECT  
  userid,
  SESSION_START(log_time, INTERVAL '10' SECOND) AS session_beg,
  SESSION_ROWTIME(log_time, INTERVAL '10' SECOND) AS session_end,
  COUNT(request_line) AS request_cnt
FROM server_logs
WHERE status_code = '403'
GROUP BY 
  userid, 
  SESSION(log_time, INTERVAL '10' SECOND);
