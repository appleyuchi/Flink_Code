import org.springframework.boot.SpringApplication
object Runner {
 
  def main(args: Array[String]): Unit = {
    SpringApplication.run(classOf[AppConf])
  }
 
}