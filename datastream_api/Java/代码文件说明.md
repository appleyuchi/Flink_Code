

①[Flink DataStream API Programming Guide](https://ci.apache.org/projects/flink/flink-docs-stable/dev/datastream_api.html)<br>

|内容|备注|
|---|---|
|[Anatomy of a Flink Program](https://gitee.com/appleyuchi/Flink_Code/blob/master/datastream_api/Java/src/main/java/AnatomyofaFlinkProgram.java)|无
|[Example Program](https://gitee.com/appleyuchi/Flink_Code/blob/master/datastream_api/Java/src/main/java/WindowWordCount.java)|DataStream版本的wordcount|
|[Iterations](https://gitee.com/appleyuchi/Flink_Code/blob/master/datastream_api/Java/src/main/java/Iterations.java)|流迭代器,根据<br>[what's the meaning of CloseWith of flink](https://stackoverflow.com/questions/63645374/whats-the-meaning-of-closewith-of-flink)<br>信息，不被checking point支持，不被推荐使用|
|[Controlling Latency](https://gitee.com/appleyuchi/Flink_Code/blob/master/datastream_api/Java/src/main/java/ControllingLatency.java)|缓冲区会在该时间之后认为超时，然后进行缓冲区的刷新
|[Local Execution Environment](https://gitee.com/appleyuchi/Flink_Code/blob/master/datastream_api/Java/src/main/java/ControllingLatency.java)|createLocalEnvironment<br>的用法，代码与上同
|[Collection Data Sources](https://gitee.com/appleyuchi/Flink_Code/blob/master/datastream_api/Java/src/main/java/CollectionDataSources.java)|存在序列化问题(无法解决,官方文档说强行使用的話并行度是1)
|[Iterator Data Sink](https://gitee.com/appleyuchi/Flink_Code/blob/master/datastream_api/Java/src/main/java/IteratorDataSink.java)|DataStream->Iterator


<br>
<br>
<br>


②[Operators](https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/stream/operators/index.html)<br>(目前进行到这里)

|算子|备注|
|---|---|
|[DataStream Generator](./src/main/java/datagen.java)|流数据生成器(自己收集的，和官方文档无关)|
|[Map](./src/main/java/Map.java)|映射|
|[FlatMap](./src/main/java/FlatMap.java)|打散+映射|
|[Filter](./src/main/java/Filter.java)|过滤|
|[Keyby](./src/main/java/KeyBy.java)|相同key的尽量聚集到同一个partition中|
|[Reduce](./src/main/java/Reduce.java)|聚合|
|[Fold](./src/main/java/Fold.java)|和reduce功能类似|
|[Aggregations](./src/main/java/Aggregations.java)|聚合统计最大最小值等|
|[Window](../../水位线实验/src/main/java/WaterMark_disorder.java)|
|[windowAll](./datastream_api/Java/src/main/java/WindowAll.java)|nc -lk 3456<br>输入以下数据:<br>hello,1553503185000<br>hello,1553503186000<br>hello,1553503187000<br>hello,1553503188000<br>hello,1553503189000<br>hello,1553503190000<br>hello,1553503187000<br>hello,1553503186000<br>hello,1553503191000<br>hello,1553503192000<br>hello,1553503193000<br>hello,1553503194000<br>hello,1553503195000
|[Window Apply](./src/main/java/WindowApply.java)|输入数据<br>test,1<br>test,2<br>chiyu,3<br>chiyu,4
|[Window Reduce](./src/main/java/WindowReduce.java)|聚合操作(代码内含测试数据)
|[Window Fold](./src/main/java/WindowFold.java)|把几个数据变成一个String数据
|[Aggregations on windows](./src/main/java/AggregationsOnWindows.java)|聚合统计|
|[Union](./src/main/java/Union.java)|合并几个datastream变量的内容
|[Window Join](./src/main/java/WindowJoin.java)|流之间的join功能
|[Interval Join](./src/main/java/IntervalJoin.java)|对于一定时间间隔内的两个datastream进行join<br>另外一个很好的例子是:<br>[Flink DataStream 基于Interval Join实时Join过去一段时间内的数据](https://blog.csdn.net/wangpei1949/article/details/103108474)
|[Window CoGroup-LeftJoin](./src/main/java/WindowCoGroup_LeftJoin.java)<br>(输入为StreamDataSource<br>StreamDataSource1)<br>[Window CoGroup-RightJoin](./src/main/java/WindowCoGroup_RightJoin.java)<br>(输入为StreamDataSource1<br>StreamDataSource2)|通过CoGroup实现LeftJoin和RightJoin
|[Connect](./src/main/java/Connect.java)|两个datastream连接成一个stream
|[CoMap, CoFlatMap](./src/main/java/CoFlatMap.java)|两个Datastream合并然后flat打散|
|[Split](./src/main/java/Split.java)|一个datastream分为多个部分
|[Select](./src/main/java/Split.java)|从SplitStream流中根据outputname选择
|[Iterate](./src/main/java/Iterate.java)|迭代流(一般不建议使用)
|[Project](./src/main/java/Project.java)|根据下标映射
|Custom partitioning(略)|
|Random partitioning(略)|
|Rebalancing (Round-robin partitioning)(略)|
|Rescaling(略)|
|Start new chain(略)|
|Disable chaining(略)|
|Set slot sharing group(略)|




④[Generating Watermarks](https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/event_timestamps_watermarks.html)<br>
下面这块不知道在什么场景有需要,先放着

|内容|备注|
|---|---|
|Introduction to Watermark Strategies||
|Using Watermark Strategies||
|Dealing With Idle Sources||
|Writing WatermarkGenerators||
|Writing a Periodic WatermarkGenerator||
|Watermark Strategies and the Kafka Connector||



⑦[Working with State](https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/stream/state/state.html)<br>

|内容|备注|
|---|---|
|[Keyed DataStream]||
|[Using Keyed State](./src/main/java/UsingKeyedState.java)|使用Keyed State的一个示例|
|[State Time-To-Live (TTL)]|
|[Cleanup of Expired State]|
|[Cleanup in full snapshot]|

\--------------------------下面暂时不处理-------------------------------


⑤[Builtin Watermark Generators](https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/event_timestamp_extractors.html)<br>



⑥[Debugging Windows & Event Time](https://ci.apache.org/projects/flink/flink-docs-release-1.11/monitoring/debugging_event_time.html)(没啥内容)

⑧[Checkpointing](https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/stream/state/checkpointing.html)<br>(一些配置,没啥内容)


\-----------------------------------下面暂时不处理-------------------------------------------------------


⑨[Queryable State Beta](https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/stream/state/queryable_state.html)<br>(这个实在跑不通,见[自己博客记录](https://yuchi.blog.csdn.net/article/details/106839221))

⑩[Python Walkthrough](https://ci.apache.org/projects/flink/flink-statefun-docs-release-2.1/getting-started/python_walkthrough.html)(这个是python的,看起来没啥用)