public   class Student
{

    public long time;
    public String id;
    public String class_;
    public String name;



    public Student(String id, String name, String class_, long time)
    {

        this.id = id;

        this.name = name;

        this.class_ = class_;

        this.time = time;

    }

    public String getId()
    {

        return id;

    }

    public void setId(String id)
    {

        this.id = id;

    }

    public String getName()
    {

        return name;

    }



    public void setName(String name)
    {

        this.name = name;

    }

    public String getClass_()
    {

        return class_;

    }

    public void setClass_(String class_)
    {

        this.class_ = class_;

    }

    public long getTime()
    {

        return time;

    }



    public void setTime(long time)
    {

        this.time = time;

    }
}

