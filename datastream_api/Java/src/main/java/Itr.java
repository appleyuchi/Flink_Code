import java.io.Serializable;
import java.util.Iterator;
import java.util.function.Consumer;

/**
     * An optimized version of AbstractList.Itr
     */
    public class Itr<E> implements Iterator<E>, Serializable {
        private Iterator<E> iterator;
        public Itr(Iterator<E> iterator){
            this.iterator=iterator;
        }

        public boolean hasNext() {
            return iterator.hasNext();
        }

        @SuppressWarnings("unchecked")
        public E next() {
           return iterator.next();
        }

        public void remove() {
            iterator.remove();
        }

        @Override
        @SuppressWarnings("unchecked")
        public void forEachRemaining(Consumer<? super E> consumer) {
            iterator.forEachRemaining(consumer);
        }

    }