import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.ArrayList;
import java.util.List;

public class Map {


    public static void main(String[] args) throws Exception
    {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
//        DataStream[Integer] = env.fromCollection(List(2,3,4,5));

        List<Integer> data = new ArrayList<>();
        data.add(1);
        data.add(2);

        DataStream<Integer> dataStream = env.fromCollection(data);

        dataStream=dataStream.map(new MapFunction<Integer, Integer>()
        {
            @Override
            public Integer map(Integer value) throws Exception
            {
                return 2 * value;
            }
        });


        dataStream.print();
        env.execute();
    }

}


//注意看下这个连接:
//https://blog.csdn.net/qq_35620501/article/details/87909859