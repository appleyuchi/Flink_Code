import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
//import org.apache.flink.api.java.tuple.Tuple2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class CollectionDataSources
{

    public static void main(String[] args) throws Exception
    {


        final StreamExecutionEnvironment env = StreamExecutionEnvironment.createLocalEnvironment();

        // Create a DataStream from a list of elements
        DataStream<Integer> myInts = env.fromElements(1, 2, 3, 4, 5);

        // Create a DataStream from any Java collection


//        env.fromElements(Tuple2.of("1", 1));
    List<Tuple2<String, Integer>> data =new ArrayList<>();
    data.add(Tuple2.of("test",1));

    DataStream<Tuple2<String, Integer>> myTuples = env.fromCollection(data);

    // Create a DataStream from an Iterator






        List<String>expectedValues= Arrays.asList("first","second");
        Iterator<String>longIt=expectedValues.iterator();
        System.out.println(longIt.next());



//下面是一些失败的代码

//    List<LongSer>l = new ArrayList<>();
//    l.add(new LongSer(1111L));
//    l.add(new LongSer(2222L));
//    l.add(new LongSer(3333L));
//    Iterator<LongSer> longIt = new ListImpl<LongSer>(l).iterator();
//    DataStream<LongSer> myLongs = env.fromCollection(longIt, LongSer.class);
//        DataStream<String>dataStream=env.fromCollection(longIt,String.class);
//        dataStream.print();




    }


}