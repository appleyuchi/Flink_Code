import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.stream.Collector;

public class FlatMap {


        public static void main(String[] args) throws Exception
        {

            StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

            DataStream<String> datastream=env.fromElements("India country", "USA Trump");


            datastream=datastream.flatMap(new FlatMapFunction<String, String>()
            {
                @Override
                public void flatMap(String value, org.apache.flink.util.Collector<String> out) throws Exception
                {

                    for(String word: value.split(" "))
                    {
                        out.collect(word);
                    }
                }
            });

            datastream.print();

            env.execute();
        }

    }


