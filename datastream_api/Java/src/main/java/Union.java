import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Union
{


    public static void main(String[] args) throws Exception
    {
        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        //读取数据

        DataStream<Tuple3<String, String, Integer>> dataStream   = env.fromElements(ENGLISH);
        DataStream<Tuple3<String, String, Integer>> otherStream1 = env.fromElements(ENGLISH1);
        DataStream<Tuple3<String, String, Integer>> otherStream2 = env.fromElements(ENGLISH2);

        dataStream.union(otherStream1, otherStream2).print();
        env.execute();


    }






//---------------------------------数据----------------------------------------------------------
    public static final Tuple3[] ENGLISH = new Tuple3[]
            {
                    //班级 姓名 成绩

                    Tuple3.of("class1","王五",70),
                    Tuple3.of("class2","赵六",50),

            };


    public static final Tuple3[] ENGLISH1 = new Tuple3[]
            {
                    //班级 姓名 成绩

                    Tuple3.of("class1","王五",70),
                    Tuple3.of("class2","赵六",50),
            };

    public static final Tuple3[] ENGLISH2 = new Tuple3[]
            {
                    //班级 姓名 成绩
                    Tuple3.of("class2","小七",40),
                    Tuple3.of("class2","小八",10),
            };


}




