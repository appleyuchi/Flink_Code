public class UserAction {

    private String userId; //用户id
    private long timestamp; //商品id

    private String behavior; //用户行为（pv, buy, cart, fav)
    private String itemId; //商品分类id
    private int price; //商品价格




    public UserAction(String userId,long timestamp,String behavior,String itemId,int price)
    {
        this.userId=userId;
        this.timestamp=timestamp;
        this.behavior=behavior;
        this.itemId=itemId;
        this.price=price;
    }

    public String getUserID() {
        return userId;
    }



    public int getProductPrice()
    {
        return price;

    }


    public String getProductID(){

        return itemId;
    }


    public String getBehavior() {
        return behavior;
    }


    public long getTimestamp() {
        return timestamp;
    }


    public String toString()
    {

        return "userId="+userId+","+"price="+price;
    }



}