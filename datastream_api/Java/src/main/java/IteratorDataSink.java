import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamUtils;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import java.util.Iterator;

public class IteratorDataSink {


    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();


        DataStream<Tuple2<String, Integer>> myResult = env.fromElements(  Tuple2.of("Adam", 17),Tuple2.of("Sarah", 23));
        Iterator<Tuple2<String, Integer>> myOutput = DataStreamUtils.collect(myResult);
        myResult.print();


        System.out.println(myOutput.next());





    }
    }
