import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

public class ListImpl<E> extends ArrayList<E> {

    private List<E>list;
    public ListImpl(List<E>list){
        this.list=list;
    }
    @Override
    public Iterator<E> iterator() {
        return new Itr<E>(list.iterator());
    }


}
