import java.io.Serializable;



public class UserActionLogPOJO implements Serializable
{
    private String userId; //用户id
    private String itemId; //商品分类id
    private int productPrice; //商品价格


    public UserActionLogPOJO (String userId,String itemId,int productPrice)
    {
        this.userId=userId;
        this.itemId=itemId;
        this.productPrice=productPrice;
    }
    /** default constructor */
    public UserActionLogPOJO ()
    {
    }
    public String getUserID()
    {
        return this.userId;
    }




    public void setProductID(String itemId)
    {
        this.itemId=itemId;
    }



    //－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    //－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－


    public void setProductPrice(int price)
    {
        this.productPrice=productPrice;

    }


    public int getproductPrice() {
        return productPrice;
    }


//－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－



//－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－


    public void setUserId(String userId) {
        this.userId = userId;
    }



    public String getUserId() {
        return userId;
    }



    public String toString()
    {

        return "userId="+userId+","+"price="+productPrice;
    }




}
