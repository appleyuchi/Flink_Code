一些小实验<br>

|语言| 文件夹 | 实验 | 
|----|-----| ----: | 
|Java|[Runoob](./Runoob) |java读写mysql | 
|Java|[window_learn](./window_learn)| sliding-count-window | 
|Java|[wordcount](./wordcount)|wordcount
|Scala|[scala_helloworld](./scala_helloworld)|Spring helloworld



实验总目录:

|实验|备注|
|---|---|
|[Flink CEP实验](flink_cep/README.md)|网络收集|
|[dataset_api](https://gitee.com/appleyuchi/Flink_Code/blob/master/dataset_api/Java/代码文件说明.md)|flink官方文档中所有dataset的api完整用例(leftOuterJoin和rightOuterJoin效果有瑕疵，钉钉群的官方技术支持也不懂)
|[DataStream_api](./datastream_api/Java/代码文件说明.md)|flink官方文档中所有datastream的api完整用例
|[水位线实验汇总](./水位线实验/README.md)|网络收集(完成)|
|[Flink的state实验](./flink_state/README.md)|尚未全部完成
|[Flink SQL Cookbook](flink-sql-cookbook/README.md)|完成
|[Table_api](table_api/Java/README.md)|完成(1.12向官方汇报一个bug,需要1.13才能解决)
|[Flink的各种读写实验](FLINK读写各种数据源/README.md)|正在进行中
|[Flink数据倾斜](Flink数据倾斜/README.md)|沒有完成
|[Flink读Kafka](./flink读kafka/README.md)|
|[Hive动态分区/静态分区用法](hive动态静态分区)|完成
|[UDF写法(官方文档)](UDF写法研究/README.md)|完成
|[flink清洗数据案例-IOUtis用法](flink清洗数据案例/IOUtis/README.md)|完成
|[flink清洗数据案例-FastJson用法](flink清洗数据案例/FastJson_Learn/README.md)|完成
|[HIVE_UDF](HIVE_UDF/README.md)|完成
|[Flink时区问题](Flink时区问题)|完成



## 数据采集要求:
收集的数据：全面，准确 ,实时
## 数据来源：
1. 客户端(ios ，android ,web端(H5,小程序等)) ，主要采集用户操作行为数据
2. 服务端: 采集客户端请求服务端的日志记录,还有前端无法采集的业务系统日志(商品信息，订单信息,货币存量,营收等后台系统数据)
3. 第三方数据: 外部数据(爬虫数据)
## 数据埋点方案设计:
目前客户端埋点(事件模型)：
who, 访客标识、设备指纹、登录ID
when ,事件发生时间、上报时间 (设计参数)
where,设备环境、网络环境、业务环境等 (设计的参数)
what ,事件标识、事件参数 (设计的参数)
有了埋点sdk之后，开发只需要关注
* SDK的初始化配置
* 事件怎么标识
* 事件需要哪些参数
* 事件如何触发
## 数据采集方式:
1. 客户端上报: sdk前端打点上报 :通过http上报 ,生成日志,写入数据仓库, 缺点:由于网络，服务异常导致传输失败,发生丢包,可以添加失败重试机制, 受版本限制,如果数据埋点出现问题,只有发版后数据才能修正。
2. 服务端通过写日志写日志 采集日志,入数据仓库，准确性高,实时性好,不受版本限制的影响,缺点：无法记录行为日志,
## 数据上报准确性校验及指标:
1. 没有误报错报,
两种情况 ：本来是应该报A,结果上报了B,有上报但是没有上报正确) ,
上报规则不正确(数据校验系统做到)，枚举型参数,本来只能上报1-5,结果上报了6
2. 没有漏报, 改上报没有上报,
3. 有没有延迟上报:
延迟上报的情景：请求数并发数过高服务器压力过大,请求处理不及时;
上报失败后重新上传的机制
(针对实时性数据或者是需要统计用户时长数据)
定义指标内容和探索验证方案:上报成功率，上报延迟率，必报字段空值率。同时针对各端SDK进行了指标口径定义、验证、指标提升和指标监控。
## 数据源存储: 存储在hive 表中,好友通讯录数据存储在Hbase中 。
日志行为数据:
服务端日志：
第三方日志:
如果要做离线计算,存储在hive中
如果对外提供接口,数据存储在mysql，hbase中
## 数据计算及存储及展示:ETL 数据仓库建模(数据宽表)
## 数据平台：针对数据开发人员
集成：服务端采集，客户端上报，文件导入
计算：HIVE查询，调度系统，MrSparkApp
可视化：BI系统()
数据管理：HIVE表查询,分析主题,数据模型,数据源,报表,统计分析
虚拟平台:ActiveMQ ,Redis，Storm，Kafka,Flink 集群
监控：短信监控,微信监控,邮件监控
数据运营平台:
数据跑数模板: 模板管理，模板权限
数据运营：用户信息查询
数据埋点管理平台：
## 数据应用：
* A/Btest实验
* 推荐系统
* 自助式数据分析平台
每一块都有很多工作需要做，都有对应的理论和方法支撑,后面会逐步更新。

